package es.ua.eps.filmoteca;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;

import java.io.Serializable;
import java.util.List;

public class FilmDataFragment extends Fragment implements FilmDataSource.AsyncTaskCallback, DriveManager.DriveManagerListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    public static final String EXTRA_FILE_TITLE = "EXTRA_FILE_TITLE";
    public static final String EXTRA_FILM_INDEX = "EXTRA_FILM_INDEX";
    public static final int ACTIVITY_EDIT_FILM = 1;

    Film film;
    int index;

    ImageView image;
    TextView titleView;
    TextView directorView;
    TextView yearView;
    TextView genreView;
    Button backButton;
    Button editButton;
    Button imdbButton;
    Button mapButton;
    GoogleApiClient googleApiClient;
    DriveManager driveManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            index = args.getInt(EXTRA_FILM_INDEX, 0);

            if (index >= 0) {
                film = MainActivity.films.get(index);
            }
        }

        googleApiClient = ((MainActivity)getActivity()).googleApiClient;

        driveManager = ((MainActivity)getActivity()).driveManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_film_data, container, false);

        image = (ImageView) v.findViewById(R.id.film_data_image);
        titleView = (TextView) v.findViewById(R.id.film_data_text);
        directorView = (TextView) v.findViewById(R.id.film_data_director);
        yearView = (TextView) v.findViewById(R.id.film_data_year);
        genreView = (TextView) v.findViewById(R.id.film_data_genre);
        backButton = (Button) v.findViewById(R.id.back_button);
        editButton = (Button) v.findViewById(R.id.edit_film_button);
        imdbButton = (Button) v.findViewById(R.id.imdb_button);
        mapButton = (Button) v.findViewById(R.id.map_button);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FilmEditActivity.class);
                intent.putExtra(FilmEditActivity.EXTRA_FILM, (Serializable) film);
                startActivityForResult(intent, ACTIVITY_EDIT_FILM);
            }
        });

        imdbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(film.getImdbURL()));
                startActivity(intent);
            }
        });

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                intent.putExtra(MapsActivity.EXTRA_FILM, (Serializable) film);
                startActivity(intent);
            }
        });

        updateView();

        return v;
    }

    private void updateView() {
        if (film != null) {
            image.setImageResource(film.getImage());
            titleView.setText(film.getTitle());
            directorView.setText(film.getDirector());
            yearView.setText(String.format("%d", film.getYear()));
            genreView.setText(String.format("%s, %s", film.getGenre(), film.getFormat()));
        }
    }

    public void setItemPosition(int position) {
        index = position;
        film = MainActivity.films.get(index);
        updateView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ACTIVITY_EDIT_FILM:
                if (resultCode == Activity.RESULT_OK) {
                    Film edited = (Film)data.getSerializableExtra(FilmEditActivity.EXTRA_FILM_EDITED);
                    updateView();

                    MainActivity.films.set(index, edited);

                    ((MainActivity)getActivity()).update = true;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onTaskEnded() {

    }

    @Override
    public void onListRead(List<Film> films) {

    }

    @Override
    public void onWriteFinished() {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}

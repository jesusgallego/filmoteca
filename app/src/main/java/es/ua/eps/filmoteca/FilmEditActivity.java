package es.ua.eps.filmoteca;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.Serializable;

public class FilmEditActivity extends AppCompatActivity {

    public static final String EXTRA_FILM = "EXTRA_FILM";
    public static final String EXTRA_FILM_EDITED = "EXTRA_FILM_EDITED";

    Film film;
    EditText title;
    EditText director;
    EditText year;
    Spinner genre;
    Spinner format;
    EditText imdb;
    EditText notes;
    EditText lat;
    EditText lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_edit);

        Intent intent = getIntent();
        film = (Film) intent.getSerializableExtra(EXTRA_FILM);

        title = (EditText) findViewById(R.id.edit_title);
        director = (EditText) findViewById(R.id.edit_director);
        year = (EditText) findViewById(R.id.edit_year);
        genre = (Spinner) findViewById(R.id.edit_genre);
        format = (Spinner) findViewById(R.id.edit_format);
        imdb = (EditText) findViewById(R.id.edit_imbd);
        notes = (EditText) findViewById(R.id.edit_notes);
        lat = (EditText) findViewById(R.id.edit_lat);
        lng = (EditText) findViewById(R.id.edit_lng);

        ArrayAdapter genreAdapter = ArrayAdapter.createFromResource(this, R.array.genre, android.R.layout.simple_spinner_item);
        genreAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genre.setAdapter(genreAdapter);
        genre.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                film.setGenre(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter formatAdapter = ArrayAdapter.createFromResource(this, R.array.format, android.R.layout.simple_spinner_item);
        formatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        format.setAdapter(formatAdapter);
        format.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                film.setFormat(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (film != null) {
            title.setText(film.getTitle());
            director.setText(film.getDirector());
            year.setText(String.format("%d", film.getYear()));
            genre.setSelection(genreAdapter.getPosition(film.getGenre()));
            format.setSelection(formatAdapter.getPosition(film.getFormat()));
            imdb.setText(film.getImdbURL());
            notes.setText(film.getNotes());
            lat.setText(String.format("%f", film.getLat()));
            lng.setText(String.format("%f", film.getLng()));
        }
    }

    private void syncObject() {
        film.setTitle(title.getText().toString());
        film.setDirector(director.getText().toString());
        film.setYear(Integer.parseInt(year.getText().toString()));

        film.setImdbURL(imdb.getText().toString());
        film.setNotes(notes.getText().toString());

        String latitudString = lat.getText().toString().replace(',', '.');
        String longitudString = lng.getText().toString().replace(',', '.');
        double latitud = Double.parseDouble(latitudString);
        double longitud = Double.parseDouble(longitudString);
        film.setLat(latitud);
        film.setLng(longitud);
    }

    public void onSave (View v) {
        syncObject();

        Intent result = new Intent();
        result.putExtra(EXTRA_FILM_EDITED, (Serializable) film);
        setResult(Activity.RESULT_OK, result);
        finish();
    }

    public void onCancel (View v) {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}

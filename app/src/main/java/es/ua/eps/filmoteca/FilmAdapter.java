package es.ua.eps.filmoteca;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mastermoviles on 20/9/16.
 */
public class FilmAdapter extends ArrayAdapter<Film> {
    public FilmAdapter(Context context, int resource, List<Film> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.film_adapter, parent, false);
        }
        Film f = getItem(position);

        ImageView image = (ImageView)convertView.findViewById(R.id.film_icon);
        TextView title = (TextView) convertView.findViewById(R.id.film_title);
        TextView director = (TextView)convertView.findViewById(R.id.film_director);

        image.setImageResource(f.getImage());
        title.setText(f.getTitle());
        director.setText(f.getDirector());

        return convertView;
    }
}

package es.ua.eps.filmoteca;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jesusgallego on 25/3/17.
 */

public class DriveManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String filename = "peliculas.txt";

    private GoogleApiClient googleApiClient;
    private DriveManagerListener callback;
    private Context context;
    private DriveId driveId;

    private String TAG = "DriveManager";

    public DriveManager(GoogleApiClient client, Context ctx, DriveManagerListener listener) {
        this.context = ctx;
        this.callback = listener;

        this.googleApiClient = client;
    }

    public void getFilms() {
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.MIME_TYPE, "text/plain"))
                .addFilter(Filters.eq(SearchableField.TITLE, filename))
                .build();

        Drive.DriveApi.getAppFolder(googleApiClient)
                .queryChildren(googleApiClient, query)
                .setResultCallback(driveReadContentsCallback);
    }

    public void addFilms(final List<Film> films) {
        WriteFileTask writeFileTask = new WriteFileTask();
        writeFileTask.execute(films);
    }

    public interface DriveManagerListener {
        void onListRead(List<Film> films);
        void onWriteFinished();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    final private ResultCallback<DriveApi.MetadataBufferResult> driveReadContentsCallback =
            new ResultCallback<DriveApi.MetadataBufferResult>() {
                @Override
                public void onResult(DriveApi.MetadataBufferResult result) {
                    // If no file created
                    if (!result.getStatus().isSuccess()) {
                        return;
                    }

                    // If  the file exists, read data
                    for (final Metadata metadata : result.getMetadataBuffer()){
                        if (metadata.isInAppFolder()){
                            //Toast.makeText(FilmListActivity.this, result, Toast.LENGTH_SHORT)
                            //        .show();
                            driveId = metadata.getDriveId();
                            new ReadFileTask().execute();
                            return;
                        }
                    }

                    Drive.DriveApi.newDriveContents(googleApiClient)
                            .setResultCallback(driveCreateFileContentsCallback);
                }
            };

    final private ResultCallback<DriveApi.DriveContentsResult> driveCreateFileContentsCallback =
            new ResultCallback<DriveApi.DriveContentsResult>() {
                @Override
                public void onResult(DriveApi.DriveContentsResult driveContentsResult) {
                    if (!driveContentsResult.getStatus().isSuccess()) {
                        // handle error
                        return;
                    }

                    DriveContents contents = driveContentsResult.getDriveContents();
                    /*try {
                        OutputStream fos = contents.getOutputStream();
                        ObjectOutputStream oos = new ObjectOutputStream(fos);

                        oos.writeObject(films);

                        oos.close();
                        fos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                    final MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                            .setTitle(filename)
                            .setMimeType("text/plain")
                            .build();

                    Drive.DriveApi.getAppFolder(googleApiClient)
                            .createFile(googleApiClient, changeSet, contents)
                            .setResultCallback(fileWriteCallback);
                }
            };

    final private ResultCallback<DriveFolder.DriveFileResult> fileWriteCallback = new
            ResultCallback<DriveFolder.DriveFileResult>() {
                @Override
                public void onResult(DriveFolder.DriveFileResult result) {
                    if (!result.getStatus().isSuccess()) {
                        return;
                    }
                    driveId = result.getDriveFile().getDriveId();
                    new ReadFileTask().execute();
                }
            };

    public class ReadFileTask extends AsyncTask<Void, Void, List<Film>> {
        @Override
        protected List<Film> doInBackground(Void... params) {
            DriveFile file = Drive.DriveApi.getFile(googleApiClient, driveId);
            DriveApi.DriveContentsResult driveContentsResult =
                    file.open(googleApiClient, DriveFile.MODE_READ_ONLY, null).await();

            if (!driveContentsResult.getStatus().isSuccess())
            {
                Log.i(TAG,"Error while trying to read file contents");
            }
            else
            {
                DriveContents contents = driveContentsResult.getDriveContents();

                List<Film> films = new ArrayList<Film>();
                try {
                    InputStream is = contents.getInputStream();
                    ObjectInputStream ois = new ObjectInputStream(is);

                    films = (List<Film>) ois.readObject();

                    ois.close();
                    is.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return films;
            }

            return new ArrayList<Film>();
        }
        @Override
        protected void onPostExecute(List<Film> films) {
            super.onPostExecute(films);

            callback.onListRead(films);
        }
    }

    public class WriteFileTask extends AsyncTask<List<Film>, Void, Void> {

        @Override
        protected Void doInBackground(List<Film>... films) {
            DriveFile file = Drive.DriveApi.getFile(googleApiClient, driveId);
            DriveApi.DriveContentsResult driveContentsResult =
                    file.open(googleApiClient, DriveFile.MODE_WRITE_ONLY, null).await();

            if (!driveContentsResult.getStatus().isSuccess()) {
                // handle error
                return null;
            }

            DriveContents contents = driveContentsResult.getDriveContents();
            try {
                OutputStream fos = contents.getOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(fos);

                oos.writeObject(films[0]);

                oos.close();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            contents.commit(googleApiClient, null);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            callback.onWriteFinished();
        }
    }

}

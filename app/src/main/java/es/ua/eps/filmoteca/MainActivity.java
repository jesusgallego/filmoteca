package es.ua.eps.filmoteca;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentContainer;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FilmListFragment.OnItemSelectedListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, DriveManager.DriveManagerListener {

    GoogleApiClient googleApiClient;
    DriveManager driveManager;

    public static List<Film> films;

    private FrameLayout fragmentContainer;
    private Bundle savedInstanceState;
    private FilmListFragment filmListFragment;
    private boolean firstLoad = true;
    public boolean update = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.savedInstanceState = savedInstanceState;

        fragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);
        filmListFragment = new FilmListFragment();

        films = new ArrayList<Film>();

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                    .requestEmail()
                    .build();

            googleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_APPFOLDER)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

        driveManager = new DriveManager(googleApiClient, this, this);


    }

    @Override
    public void onItemSelected(int position) {
        FilmDataFragment dataFragment = (FilmDataFragment) getSupportFragmentManager().findFragmentById(R.id.detail_fragment);

        if (dataFragment != null) {
            dataFragment.setItemPosition(position);
        }
        else {
            dataFragment = new FilmDataFragment();
            Bundle args = new Bundle();
            args.putInt(FilmDataFragment.EXTRA_FILM_INDEX, position);
            dataFragment.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.fragment_container, dataFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("MainActivity", "onConnected");
        if (fragmentContainer != null) {
            if(savedInstanceState != null) {
                return;
            }
            if (firstLoad) {
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, filmListFragment).commit();
                firstLoad = false;
            }
            if (update) {
                update = false;
                driveManager.addFilms(films);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onListRead(List<Film> newFilms) {
        films = newFilms;
        filmListFragment.updateAdapter();
    }

    @Override
    public void onWriteFinished() {
        driveManager.getFilms();
    }
}

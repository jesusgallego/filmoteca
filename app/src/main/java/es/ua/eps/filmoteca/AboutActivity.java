package es.ua.eps.filmoteca;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.MainThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.api.OptionalPendingResult;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView user = (TextView) findViewById(R.id.about_username);
        ImageView avatar = (ImageView) findViewById(R.id.about_avatar);

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(LoginActivity.googleApiClient);
        if (opr.isDone()) {
            Log.i("About", "isDone");
            GoogleSignInResult result = opr.get();
            GoogleSignInAccount account = result.getSignInAccount();

            user.setText(account.getDisplayName());
            avatar.setImageURI(account.getPhotoUrl());
        } else {
            Log.i("About", "notDone");
        }
    }

    public void toast (View v) {
        Toast.makeText(AboutActivity.this, R.string.not_implemented, Toast.LENGTH_SHORT).show();
    }

    public void goToWeb (View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://twitter.com/jesusgallego12"));
        startActivity(intent);
    }

    public void goToSupport (View v) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:jesusgallego@outlook.com"));
        startActivity(intent);
    }

    public void goBack (View v) {
        finish();
    }
}

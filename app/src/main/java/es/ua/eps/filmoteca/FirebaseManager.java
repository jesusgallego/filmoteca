package es.ua.eps.filmoteca;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;

/**
 * Created by mastermoviles on 2/4/17.
 */

public class FirebaseManager extends FirebaseMessagingService implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, DriveManager.DriveManagerListener {

    private final String TAG = "Firebase";
    private Film film;
    private String operation;

    private GoogleApiClient googleApiClient;
    private String filename = "peliculas.txt";
    private DriveManager driveManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.i(TAG, "Message received from: " + remoteMessage.getFrom());

        driveManager = new DriveManager(googleApiClient, getBaseContext(), this);

        if (remoteMessage.getData().size() > 0) {
            film = new Film();
            Map<String, String> data = remoteMessage.getData();
            film.setTitle(data.get("title"));
            film.setDirector(data.get("director"));
            film.setFormat(data.get("format"));
            film.setGenre(data.get("genre"));
            film.setYear(Integer.parseInt(data.get("year")));
            film.setImdbURL(remoteMessage.getData().get("imdb"));
            film.setLat(Float.parseFloat(data.get("lat")));
            film.setLng(Float.parseFloat(data.get("lng")));

            operation = remoteMessage.getData().get("operation");

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                    .requestEmail()
                    .build();

            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_APPFOLDER)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            googleApiClient.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        //startActivity(intent);
    }

    private Film findFilm(List<Film> films, String title) {
        for (Film f: films) {
            if (f.getTitle().equals(title)) {
                return f;
            }
        }
        return null;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        driveManager.getFilms();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onListRead(List<Film> films) {

        Film f = findFilm(films, film.getTitle());

        if (operation.equals("delete")) {
            if (f != null) {
                films.remove(f);
            }
        } else if (operation.equals("add")) {

            if (f == null) {
                films.add(film);
            } else {
                int index = films.indexOf(f);
                films.set(index, film);
            }
        }
        driveManager.addFilms(films);
    }

    @Override
    public void onWriteFinished() {
        Log.i(TAG, "Database updated");
    }
}

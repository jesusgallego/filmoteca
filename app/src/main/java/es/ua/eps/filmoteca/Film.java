package es.ua.eps.filmoteca;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by mastermoviles on 19/9/16.
 */
public class Film implements Serializable {

    private String title;
    private String director;
    private int year;
    private String genre;
    private String format;
    private String imdbURL;
    private String notes;
    private int image;
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }

    public int getYear() {
        return year;
    }

    public String getGenre() {
        return genre;
    }

    public String getFormat() {
        return format;
    }

    public String getImdbURL() {
        return imdbURL;
    }

    public String getNotes() {
        return notes;
    }

    public int getImage() {
        return image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setImdbURL(String imdbURL) {
        this.imdbURL = imdbURL;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public Film() {
        this.title = "No Title";
        this.director = "";
        this.year = 0;
        this.genre = "";
        this.format = "";
        this.imdbURL = "";
        this.notes = "";
        this.image = 0;
        this.lat = 0.0d;
        this.lng = 0.0d;
    }

    public Film(String title, String director, int year, String genre, String format, String imdbURL, String notes, int image, float lat, float lng) {
        this.title = title;
        this.director = director;
        this.year = year;
        this.genre = genre;
        this.format = format;
        this.imdbURL = imdbURL;
        this.notes = notes;
        this.image = image;
        this.lat = lat;
        this.lng = lng;
    }
/*
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(director);
        dest.writeInt(year);
        dest.writeString(genre);
        dest.writeString(format);
        dest.writeString(imdbURL);
        dest.writeString(notes);
        dest.writeInt(image);
    }
*/
    private Film(Parcel in) {
        title = in.readString();
        director = in.readString();
        year = in.readInt();
        genre = in.readString();
        format = in.readString();
        imdbURL = in.readString();
        notes = in.readString();
        image = in.readInt();
    }

    public static final Parcelable.Creator<Film> CREATOR = new Parcelable.Creator<Film>() {
        public Film createFromParcel(Parcel in) {
            return new Film(in);
        }
        public Film[] newArray(int size) {
            return new Film[size];
        }
    };

    @Override
    public String toString() {
        return String.format("Title: %s, Director: %s, Year: %d", title, director, year);
    }
}

package es.ua.eps.filmoteca;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class FilmListFragment extends ListFragment implements FilmDataSource.AsyncTaskCallback {

    FilmAdapter adapter;

    OnItemSelectedListener callback;
    GoogleApiClient googleApiClient;
    DriveManager driveManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        adapter = new FilmAdapter(getContext(), R.layout.film_adapter, MainActivity.films);
        setListAdapter(adapter);

        googleApiClient = ((MainActivity)getActivity()).googleApiClient;

        driveManager = ((MainActivity)getActivity()).driveManager;
        driveManager.getFilms();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_film_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final ListView listView = getListView();

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            private int selected = 0;
            List<Film> toDelete = new ArrayList<Film>();

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                if (checked) {
                    selected++;
                    toDelete.add(MainActivity.films.get(position));
                }
                else {
                    selected--;
                    toDelete.remove(MainActivity.films.get(position));
                }

                mode.setTitle(selected + " " + getText(R.string.films_selected));
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_delete:
                        for (Film f : toDelete) {
                            boolean removed = MainActivity.films.remove(f);
                        }
                        updateFilms();
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                selected = 0;
                toDelete = new ArrayList<Film>();
            }

        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            callback = (OnItemSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(String.format("%s %s OnItemSelectedListener",context.toString(), R.string.must_implement));
        }
    }

    private void updateFilms() {
        //FilmDataSource  task = new FilmDataSource(getContext(), this, FilmDataSource.WRITE);
        //task.setProgressBar((ProgressBar) getActivity().findViewById(R.id.progress_bar));
        //task.execute(films);
        driveManager.addFilms(MainActivity.films);
    }

    private void disconnectApp() {
        Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                getActivity().finish();
            }
        });
    }

    private void signOut() {

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                getActivity().finish();
            }
        });
    }

    public void updateAdapter() {
        adapter = new FilmAdapter(getContext(), R.layout.film_adapter, MainActivity.films);
        setListAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        callback.onItemSelected(position);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.film_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.menu_add_film:
                Film newFilm = new Film();
                MainActivity.films.add(newFilm);

                //FilmDataSource  task = new FilmDataSource(getActivity(), this, FilmDataSource.WRITE);
                //task.setProgressBar((ProgressBar) getActivity().findViewById(R.id.progress_bar));
                //task.execute(films);
                driveManager.addFilms(MainActivity.films);

                return true;
            case R.id.menu_about:
                Intent intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_disconnect:
                disconnectApp();
                return true;
            case R.id.menu_signout:
                signOut();
                return true;
        }

        return false;
    }

    @Override
    public void onTaskEnded() {
        adapter.clear();
        adapter.addAll(MainActivity.films);
        adapter.notifyDataSetChanged();
    }

    public interface OnItemSelectedListener {
        void onItemSelected(int position);
    }
}

package es.ua.eps.filmoteca;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mastermoviles on 20/9/16.
 */
public class FilmDataSource extends AsyncTask<List<Film>, Integer, List<Film>> {
    //public static List<Film> films = new ArrayList<Film>();
    public static final String filename = "peliculas.dat";

    public static final String WRITE = "WRITE";
    public static final String READ = "READ";

    private Context context;
    private AsyncTaskCallback callback;
    private String type;
    private ProgressBar progressBar;

    public FilmDataSource(Context context, AsyncTaskCallback callback, String type) {
        this.context = context;
        this.type = type;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (this.progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.progressBar != null) {
            progressBar.setProgress(values[0]);
        }
    }

    @Override
    protected final List<Film> doInBackground(List<Film>... params) {
        switch (type) {
            case READ:
                return getFilms();
            case WRITE:
                addFilms(params[0]);
                break;
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Film> salida) {
        super.onPostExecute(salida);
        if (this.progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
        callback.onTaskEnded();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (this.progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    private List<Film> getFilms() {
        List<Film> films = new ArrayList<Film>();
        try {

            FileInputStream in = context.openFileInput(filename);

            ObjectInputStream ois = new ObjectInputStream(in);

            films = (List<Film>) ois.readObject();

            ois.close();
            in.close();

            Thread.sleep(500);
        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            e.printStackTrace();
        }

        return films;
    }

    private void addFilms(List<Film> films) {
        try {
            FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);

            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(films);

            oos.close();
            fos.close();

            Thread.sleep(500);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public interface AsyncTaskCallback {
        public void onTaskEnded();
    }

}
